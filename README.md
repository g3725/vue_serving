# Vue Serving

⚠️ This service is deprecated. Please use https://gitlab.com/moreillon_ci/api_proxy from hereon⚠️

[![dockeri.co](https://dockeri.co/image/moreillon/vue-serving)](https://hub.docker.com/r/moreillon/vue-serving)

A service used to serve Vue.js single page applications